#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/sendfile.h>


int ret;
void retchk(const char *str) {
    if (ret < 0) {
        perror(str);
        abort();
    }
}

int main(int argc, char **argv) {
    // Get args
    char *host, *filename;
    int port;
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <host> <port> <binary file>\n", argv[0]);
        return EXIT_FAILURE;
    }
    host = argv[1];
    port = atoi(argv[2]);
    filename = argv[3];

    // Open and get file size
    int file;
    uint64_t file_size;
    {
        file = ret = open(filename, O_RDONLY);
        retchk("open");
        struct stat file_stat;
        ret = fstat(file, &file_stat);
        retchk("fstat");
        file_size = file_stat.st_size;
    }

    // Create socket
    int sock = ret = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    retchk("socket");

    // Get addrs
    struct hostent *hostaddr;
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    hostaddr = gethostbyname(host);
    memcpy(&servaddr.sin_addr, hostaddr->h_addr, hostaddr->h_length);

    // Connect to server
    ret = connect(sock, (struct sockaddr *)&servaddr, sizeof(servaddr));
    retchk("connect");

    // Send file size
    ret = write(sock, &file_size, sizeof(file_size));
    retchk("write file_size");

    // Send file
    ret = sendfile(sock, file, NULL, file_size);
    retchk("sendfile");

    // Create epoll
    struct epoll_event ev = {
        .events = EPOLLIN,
        .data = {
            .fd = sock
        }
    };
    int epollfd = epoll_create(1);
    epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev);
    struct epoll_event epoll_events[1];

    // Print data received back
    fflush(stdout);
    for (;;) {
        // Epoll wait
        ret = 0;
        while (ret == 0) {
            ret = epoll_wait(epollfd, epoll_events, 1, -1);
            retchk("epoll_wait");
        }
        // Read some
        char buf[20];
        ret = read(sock, buf, sizeof(buf));
        retchk(NULL);
        // Process
        if (ret == 0) {
            break;
        } else {
            // write to stdout
            write(STDOUT_FILENO, buf, ret);
        }
    }
}
